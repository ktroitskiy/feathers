// Initializes the `cityes` service on path `/cityes`
const createService = require('feathers-nedb');
const createModel = require('../../models/cityes.model');
const hooks = require('./cityes.hooks');
const filters = require('./cityes.filters');

module.exports = function () {
  const app = this;
  const Model = createModel(app);
  const paginate = app.get('paginate');

  const options = {
    name: 'cityes',
    Model,
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/cityes', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('cityes');

  service.hooks(hooks);

  if (service.filter) {
    service.filter(filters);
  }
};
