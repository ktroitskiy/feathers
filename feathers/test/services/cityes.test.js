const assert = require('assert');
const app = require('../../src/app');

describe('\'cityes\' service', () => {
  it('registered the service', () => {
    const service = app.service('cityes');

    assert.ok(service, 'Registered the service');
  });
});
