import React, { Component } from 'react'
import { connect } from 'react-redux'

import CityesList from './components/CityesList/CityesList'

import './App.css';
import { addItem, getList, editItem, deleteItem } from './actions/cityes';

class App extends Component {
	render() {
    return (
      <div>
        <CityesList items = {this.props.items}
                    onAddItem = {this.props.onAddItem}
                    onGetList = {this.props.onGetList}
                    onEditItem = {this.props.onEditItem}
                    onDeleteItem = {this.props.onDeleteItem}
        />
      </div>
    );
  }
}

export default connect(
  state => ({
    items: state,
  }),
  dispatch => ({
    onEditItem: (item) => {
      dispatch(editItem(item));
    },
    onDeleteItem: (item) => {
      dispatch(deleteItem(item));
    },
    onAddItem: (item) => {
      dispatch(addItem(item));
    },
    onGetList: () => {
      dispatch(getList());
    }
  })
)(App)
