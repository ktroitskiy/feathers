import axios from 'axios'

var url = 'http://localhost:3030/cityes'

const itemFetchSuccess = ({ data }) => {
  return {
    type: 'FETCH_LIST_SUCCESS',
    payload: Object.values(data).reduce((result, item) => {
      result[item._id] = item
      return result
    }, {})
  }
}

export const getList = () => dispatch => {
  axios.get(url)
  .then(response => {
    dispatch(itemFetchSuccess(response.data))
  })
  .catch(function (error) {
    console.log(error);
  })
}


export const editItem = (item) => dispatch => {
  axios.patch(url + '_id=' + item.id, {
    name: item.name,
    description: item.description
  })
  .then(response => {

  })
  .catch(e=> {
      this.errors.push(e)
  })
}

export const addItem = (item) => dispatch => {

  axios.post(url, {
    name: item.name,
    description: item.description
  })
  .then(response => {})
  .catch(e=> {
      this.errors.push(e)
  })
}

export const deleteItem = (item) => dispatch => {
  axios.delete(url, {
    _id: item._id
  })
}

export const activeItem = id => {
  return {
    type: 'ITEM_ACTIVE',
    payload: id
  }
}
