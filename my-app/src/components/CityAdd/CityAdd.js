import React, { Component } from 'react';

class CityAdd extends Component {
    

    addItem() {
        var item = {
            name: this.itemInputName.value,
            description: this.itemInputDescription.value,
        }

        console.log("Added", item)
        this.props.onAddItem(item)
    }

    render() {
        return(
            <div className="col-md-6">
                <h2>Добавить</h2>
                <form onSubmit={this.addItem.bind(this)}>
                    <label>Name</label><br />
                    <input type='text' name='name' ref={(input) => {this.itemInputName = input}} />
                    <br /><br />
                    <label>Description</label><br />
                    <input name='description' ref={(input) => {this.itemInputDescription = input}} />
                    <br /><br />
                    <input type="submit" value="Отправить" />
                </form>
                <br />
            </div>
        )
    }
}

export default CityAdd