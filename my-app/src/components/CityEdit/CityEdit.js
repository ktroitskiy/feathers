import React, { Component } from 'react';
import { connect } from 'react-redux'

class CityEdit extends Component {

    editItem() {
        var item = {
            id: this.props.items._id,
            name: this.itemInputName.value,
            description: this.itemInputDescription.value,
        }
        this.props.onEditItem(item)
    }

    deleteItem() {
        this.props.onDeleteItem(this.props.item)
    }

    render() {

        const { item = {} } = this.props

        return (
            <div className="col-md-6">
                <h2>Редактировать</h2>
                <form onSubmit={this.editItem.bind(this)}>
                    <label>Name: </label><br />
                    <input name='name' value={item.name || ''} ref={(input) => {this.itemInputName = input}} placeholder="new name" />
                    <br /><br />
                    <label>Description: </label><br />
                    <input name='description' value={item.description || ''} ref={(input) => {this.itemInputDescription = input}} placeholder="new description"/>
                    <br /><br />
                    <input type="submit" value="Отправить" />
                </form>
                <br />
                <form onSubmit={this.deleteItem.bind(this)}>
                    <input type="submit" value="Удалить" />
                </form>
            </div>
        )
    }
}

const mapStateToProps = state => {

    const { activeId } = state

    return {
        item: state.items[activeId]
    }
}

export default connect(mapStateToProps)(CityEdit)