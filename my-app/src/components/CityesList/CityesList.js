import React, { Component } from 'react';
import { connect } from 'react-redux'

import CityAdd from '../CityAdd/CityAdd'
import CityEdit from '../CityEdit/CityEdit'

import Pnotify from 'pnotify'

import { activeItem } from '../../actions/cityes'

class CityesList extends Component {

    componentDidMount() {
        let notice = new Pnotify({
            title: 'Confirmation',
            text: '<p>You are about to permanently delete this user from the database. Are you sure you want to proceed?</p>',
            hide: false,
            confirm: {
              confirm: true,
              buttons: [
                {
                  text: 'Yes',
                  addClass: 'btn btn-sm btn-primary'
                },
                {
                  text: 'No',
                  addClass: 'btn btn-sm btn-link'
                }
              ]
            },
            buttons: {
              closer: true,
              sticker: false
            },
            history: {
              history: false
            }
        })
        return this.props.onGetList()
    }


    handleDelete() {
        
    }


    handleInProgress() {

        new Pnotify({
          title: 'Hi!',
          text: 'This functionality is under construction',
          addclass: 'bg-primary',
          buttons: {
            closer: true
          }
        })
    
      }

    render() {

        const { dispatch, items } = this.props

        return (
                <div className="container mt3">
                    <br /><br />
                    <table className="table">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Description</th>
                            </tr>
                        </thead>
                        <tbody>
                            {Object.values(items).map((item, i) => {
                                // result[item._id] = item
                                //return result 
                                return (
                                    <tr onClick={ () => dispatch(activeItem(item._id)) } item={item} key={i}>  
                                        <td>{item.name}</td>
                                        <td>{item.description}</td> 
                                    </tr>
                                    );
                                })
                            }
                        </tbody>
                    </table>
                    <div className="row">
                        <CityAdd  onAddItem = {this.props.onAddItem} />
                        <CityEdit onEditItem = {this.props.onEditItem}
                                  onDeleteItem = {this.props.onDeleteItem}
                                  items = {this.props.items}
                                   />
                    </div>
                </div>

                
            )
    }
}

const mapStateToProps = state => {

    return {
        items: state.items
    }

}

CityesList = connect(mapStateToProps)(CityesList)

export default CityesList
