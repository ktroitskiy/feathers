import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension'

import PNotify from 'pnotify'

import App from './App'
import './index.css'

const initState = {

    items: {},
    activeId: null,

}

function reducer(state = initState, action) {

    switch (action.type) {

        case ('FETCH_LIST_SUCCESS') : {
            return {
                ...state,
                items: { ...state.items, ...action.payload }
            }
        }

        case ('ITEM_ACTIVE'): {
            return {
                ...state,
                activeId: action.payload
            }
        }

        default:
            return state

    }

}

const store = createStore(reducer, composeWithDevTools(applyMiddleware(thunk)))

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
)

